/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include "image_dial_widget.h"
#include "testwidget.h"

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QGroupBox *groupBox;
    QComboBox *comboBox;
    QLabel *label;
    QLabel *label_2;
    QComboBox *comboBox_2;
    TestWidget *dial;
    QComboBox *comboBox_3;
    QLabel *label_3;
    ImageDialWidget *dial_2;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QString::fromUtf8("Dialog"));
        Dialog->resize(573, 306);
        groupBox = new QGroupBox(Dialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 421, 231));
        groupBox->setFlat(false);
        comboBox = new QComboBox(groupBox);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setGeometry(QRect(50, 30, 101, 22));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 30, 41, 16));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 60, 41, 16));
        comboBox_2 = new QComboBox(groupBox);
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));
        comboBox_2->setGeometry(QRect(50, 60, 101, 22));
        dial = new TestWidget(groupBox);
        dial->setObjectName(QString::fromUtf8("dial"));
        dial->setGeometry(QRect(160, 20, 91, 91));
        dial->setMouseTracking(false);
        dial->setAutoFillBackground(false);
        dial->setInputMethodHints(Qt::ImhNone);
        dial->setMinimum(-100);
        dial->setMaximum(100);
        dial->setSingleStep(1);
        dial->setTracking(true);
        dial->setOrientation(Qt::Horizontal);
        dial->setInvertedAppearance(false);
        dial->setNotchesVisible(true);
        comboBox_3 = new QComboBox(groupBox);
        comboBox_3->setObjectName(QString::fromUtf8("comboBox_3"));
        comboBox_3->setGeometry(QRect(50, 90, 101, 22));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 90, 41, 16));
        dial_2 = new ImageDialWidget(groupBox);
        dial_2->setObjectName(QString::fromUtf8("dial_2"));
        dial_2->setGeometry(QRect(160, 110, 91, 91));

        retranslateUi(Dialog);

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QApplication::translate("Dialog", "Dialog", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("Dialog", "Timbre", 0, QApplication::UnicodeUTF8));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("Dialog", "SINE", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("Dialog", "TRIANGLE", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("Dialog", "SAW", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("Dialog", "SQUARE", 0, QApplication::UnicodeUTF8)
        );
        label->setText(QApplication::translate("Dialog", "OSC1", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Dialog", "OSC2", 0, QApplication::UnicodeUTF8));
        comboBox_2->clear();
        comboBox_2->insertItems(0, QStringList()
         << QApplication::translate("Dialog", "SINE", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("Dialog", "TRIANGLE", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("Dialog", "SAW", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("Dialog", "SQUARE", 0, QApplication::UnicodeUTF8)
        );
#ifndef QT_NO_WHATSTHIS
        comboBox_2->setWhatsThis(QApplication::translate("Dialog", "<html><head/><body><p><br/></p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
#ifndef QT_NO_ACCESSIBILITY
        dial->setAccessibleName(QApplication::translate("Dialog", "Mix", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_ACCESSIBILITY
        comboBox_3->clear();
        comboBox_3->insertItems(0, QStringList()
         << QApplication::translate("Dialog", "Fade", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("Dialog", "Sync", 0, QApplication::UnicodeUTF8)
        );
#ifndef QT_NO_WHATSTHIS
        comboBox_3->setWhatsThis(QApplication::translate("Dialog", "<html><head/><body><p><br/></p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        label_3->setText(QApplication::translate("Dialog", "Mode", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_ACCESSIBILITY
        dial_2->setAccessibleName(QApplication::translate("Dialog", "OSC1", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        dial_2->setAccessibleDescription(QApplication::translate("Dialog", "icons/sine_icon.svg.png", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_ACCESSIBILITY
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
