#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include "moremath.h"
#include <vector>
#include <jack/jack.h>
#include <jack/midiport.h>
#include "voice.h"
#include <QApplication>
#include <QLabel>
#include <QDial>
#include "gui/oscilloscopewidget.h"
#include "gui/timbre_panel.h"
#include <QtCore>
#include <QtGui>

using namespace std;

jack_client_t* client;
jack_port_t* midiInputPort;
jack_port_t* audioOutputPort;
jack_nframes_t sampleRate = 0;

vector<Voice> voices;
OscilloscopeWidget* scope;


void handleNoteOn(uint8_t channel, uint8_t noteNumber, uint8_t velocity)
{
	//find a voice not currently in use and use it
	for(unsigned int i = 0; i < voices.size(); i++)
	{
		if(voices[i].getState() == Voice::STATE_OFF)
		{
			voices[i].press(noteNumber, velocity);
			return;
		}
	}
}

void handleNoteOff(uint8_t channel, uint8_t noteNumber, uint8_t velocity)
{
	for(unsigned int i = 0; i < voices.size(); i++)
	{
		if(voices[i].getState() == Voice::STATE_PRESSED && voices[i].getId() == noteNumber)
		{
			voices[i].release(velocity);
			return;
		}
	}
}

void handlePitchBend(uint8_t channel, uint8_t lsb, uint8_t msb)
{
	for(unsigned int i = 0; i < voices.size(); i++)
	{
		voices[i].setPitchBend(msb / 128.0f);
	}
}

void handleControlChange(uint8_t channel, uint8_t controlNumber, uint8_t controlValue)
{

}

void handleMidiEvent(uint8_t command, uint8_t data1, uint8_t data2)
{
	if(command >= 128 && command <= 143)
	{
		handleNoteOff(command % 128 + 1, data1, data2);
		return;
	}

	if(command >= 144 && command <= 159)
	{
		handleNoteOn(command % 144 + 1, data1, data2);
		return;
	}

	if(command >= 224 && command <= 239)
	{
		handlePitchBend(command % 224, data1, data2);
		return;
	}

	if(command >= 176 && command <= 191)
	{
		handleControlChange(command % 176, data1, data2);
		return;
	}

	cout << (int)command << endl;
}

//called by jack when it's ready for the next buffer
//hint: jack_nframes_t is a typedef for uint32_t
float phase = 0;
int processCallback(jack_nframes_t bufferSize, void* args)
{
	//check the midi buffer for new messages
	//remember: buffer pointers should not be cached according to the api docs
	void* midiInputBuffer = jack_port_get_buffer(midiInputPort, bufferSize);

	jack_midi_event_t midiEvent;
	jack_nframes_t eventCount = jack_midi_get_event_count(midiInputBuffer);
	for(unsigned int i = 0; i < eventCount; i++)
	{
		jack_midi_event_get(&midiEvent, midiInputBuffer, i);
		handleMidiEvent(
			(uint8_t)(long)midiEvent.buffer[0],
			(uint8_t)(long)midiEvent.buffer[1],
			(uint8_t)(long)midiEvent.buffer[2]);
	}

	//get audio buffer
	jack_default_audio_sample_t* audioOutputBuffer = 
                (jack_default_audio_sample_t *) 
                jack_port_get_buffer (audioOutputPort, bufferSize);

    //clear the buffer
    memset(audioOutputBuffer, 0, bufferSize * sizeof(float));

    //process all voices
    for(int i = 0; i < voices.size(); i++)
    	voices[i].addToOutput(audioOutputBuffer, bufferSize);

    //display buffer in scope
    scope->setBuffer(audioOutputBuffer, bufferSize);
	
	return 0; 
}

//called by jack when it changes the sample rate
int setSampleRateCallback(jack_nframes_t newSampleRate, void* args)
{
	cout << "Sample rate changed from " << sampleRate << " to " << newSampleRate << endl;
	for(int i = 0; i < voices.size(); i++)
	{
		voices[i].setSampleRate(newSampleRate);
	}
	sampleRate = newSampleRate;

	return 0;
}

void errorCallback(const char* description)
{
	cout << "Jack encountered an error; closing." << endl;
	cout << description << endl << flush;
	exit(99);
}

//called by jack when this client is shut down
void shutdownCallback(void* args)
{
	cout << "Received shutdown signal; closing." << endl << flush;

	exit(0);
}

//set up our client and connect to the jack audio server
void audioServerConnect()
{
	//set this one up first to catch any problems
	jack_set_error_function(errorCallback);

	client = jack_client_open("multigrain", JackNoStartServer, NULL);
	if(client == 0)
	{
		cout << "Could not connect to jack.  Is it running?" << endl << flush;
		exit(1);
	}

	jack_set_process_callback(client, processCallback, NULL);
	jack_set_sample_rate_callback(client, setSampleRateCallback, NULL);
	jack_on_shutdown(client, shutdownCallback, NULL);

	midiInputPort =	jack_port_register(client, "input", JACK_DEFAULT_MIDI_TYPE, JackPortIsInput | JackPortIsTerminal, 0);
	audioOutputPort =	jack_port_register(client, "output", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput | JackPortIsTerminal, 0);

	if(jack_activate(client))
	{
		cout << "Could not activate client." << endl;
		exit(2);
	}

	const char** ports;
	//list all output ports (devices that can be read)
	if ((ports = jack_get_ports(client, NULL, NULL, JackPortIsOutput)) == NULL)
	{
        cout << "Cannot find any input devices." << endl;
        exit(3);
    }

    //iterate through port list
    int portCount = 0;
    while(1)
	{
		//list is null-terminated
		if(ports[portCount] == NULL) break;

		//connect to all midi ports
		string portType = string(jack_port_type(jack_port_by_name(client, ports[portCount])));
		if(portType.find("midi") != string::npos)
		{
			cout << "Connecting to midi input port: " << ports[portCount] << endl;
			if (jack_connect(client, ports[portCount], jack_port_name(midiInputPort)))
		    {
		        cout << "Cannot connect to input device." << endl << flush;
		        exit(3);
		    }
		}

		portCount++;
	}

    jack_free(ports);

	//list all physical input ports (devices that can be written to)
	if ((ports = jack_get_ports(client, NULL, NULL,
		JackPortIsPhysical | JackPortIsInput)) == NULL)
	{
        cout << "Cannot find any output devices." << endl << flush;
        exit(3);
    }

    portCount = 0;
    while(1)
	{
		//list is null-terminated
		if(ports[portCount] == NULL) break;

		//connect to all non-midi ports
		string portType = string(jack_port_type(jack_port_by_name(client, ports[portCount])));
		if(portType.find("midi") == string::npos)
		{
			cout << "Connecting to audio output port: " << ports[portCount] << endl;
			if (jack_connect(client, jack_port_name(audioOutputPort), ports[portCount]))
		    {
		        cout << "Cannot connect to output device." << endl << flush;
		        exit(3);
		    }
		}

		portCount++;
	}

    jack_free(ports);
}

void setUpGUI()
{
	//main window
	QWidget* mainWidget = new QWidget();
	mainWidget->setStyleSheet("background-color: #202020");
	QVBoxLayout* mainLayout = new QVBoxLayout();
	mainLayout->setSpacing(0);
	mainWidget->setLayout(mainLayout);

	//timbre
    TimbrePanel* timbrePanel = new TimbrePanel();
	mainLayout->addWidget(timbrePanel);

	//scope
	scope = new OscilloscopeWidget();
	mainLayout->addWidget(scope);

	mainWidget->show();
}

int main(int argc, char* argv[])
{
	cout << "Multigrain starting..." << endl;

	voices.reserve(Voice::MAX_POLYPHONY);
	for(int i = 0; i < Voice::MAX_POLYPHONY; i++)
	{
		voices.push_back(Voice());
	}

	QApplication app(argc, argv);
	setUpGUI();
	
    cout << "Connecting to jack..." << endl;;
	audioServerConnect();
	cout << "Connected to Jack." << endl;

	int qReturn = app.exec();	

	cout << "Recieved shutdown signal.  Disconnecting..." << endl;

	jack_client_close(client);

	cout << "Disconnected from jack." << endl;

	return qReturn;
}
