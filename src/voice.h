#ifndef VOICE_H
#define VOICE_H

#include <stdint.h>
#include <math.h>
#include "oscillator.h"
#include "envelope.h"

class Voice
{
public:
	//constants
	static const int MAX_POLYPHONY = 20;
	static const char STATE_OFF = 0;
	static const char STATE_PRESSED = 1;
	static const char STATE_RELEASING = 2;

	//instance functions
	Voice();
	void addToOutput(float* outputBuffer, uint32_t bufferSize);
	void setADSR(float attackTime, float decayTime, float sustainLevel, float releaseLevel);
	void press(uint8_t noteNumber, uint8_t velocity);
	void release(float velocity);
	char getState();
	uint8_t getId();
	void setPitchBend(float newPitchBend);
	void setSampleRate(uint32_t newSampleRate){sampleRate = newSampleRate;}

private:

	void updateFrequency();
	uint8_t noteNumber;		//this is the midi note number from the trigger press
	char state;				//state of this voice e.g. on, off, sustained
	Oscillator grainOsc1;
	Oscillator grainOsc2;
	Envelope envelope;
	float syncPhaseAcc;
	float syncPhaseInc;
	float amplitude;
	uint32_t sampleRate;
	float pitchBend;
};

#endif
