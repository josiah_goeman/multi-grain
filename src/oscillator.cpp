#include "oscillator.h"
#include <math.h>
#include <stdlib.h>
#include <iostream>

Oscillator::Oscillator()
{
	shape = TRI_SHAPE;
	phaseAcc = 0.0;
	amplitude = 1.0f;
}

float Oscillator::evaluate()
{
	amplitude = 1;//-= amplitude / 2;

	//phaseAcc stores the position in the curent cycle in the range 0-1
	phaseAcc += phaseInc;
	if(phaseAcc > 1.0f) phaseAcc -= 1.0f;

	float sample;
	switch(shape)
	{
		case SINE_SHAPE:
			sample = sin(phaseAcc * TWO_PI);
			break;

		case TRI_SHAPE:
			sample = 1.0f - fabs(phaseAcc - 0.5f) * 4.0f;
			break;

		case SAW_SHAPE:
			sample = -1.0f + phaseAcc * 2.0f;
			break;

		case SQUARE_SHAPE:
			sample = phaseAcc > 0.5f ? 1.0f : -1.0f;
			break;
	}

	return sample * amplitude;
}

void Oscillator::setFrequency(float newFrequency, uint32_t sampleRate)
{
	phaseAcc = 0;
	phaseInc = newFrequency / sampleRate;
}

void Oscillator::setShape(int newShape)
{
	this->shape = newShape;
}

void Oscillator::reset()
{
	phaseAcc = 0;
	amplitude = 1.0f;
}
