#ifndef MOREMATH_H
#define MOREMATH_H

#include <math.h>
#include <algorithm>
#include <stdint.h>

//#define PI 3.14159265359
//#define TWO_PI 6.28318530718

//linear interpolation
inline float lerp(float from, float to, float ratio)
{
	return (1 - ratio) * from + ratio * to;
}

template<typename real>
inline real clamp(real value, real minimum, real maximum)
{
    return std::max(std::min(value, maximum), minimum);
}

//returns the frequency of the midi note assuming a440 tuning
inline float midiToFrequency(uint8_t midiNumber)
{
	//the frequency of each note on a piano is 2^(1/12) times the frequency of the previous note
	//we use a = 440hz as a baseline
	//subtract 69 from the midi number because a4 is the 69th note
	return 440 * pow(2, (midiNumber - 69) / 12.0f);
}

#endif
