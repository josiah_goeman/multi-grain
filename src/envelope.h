#ifndef ENVELOPE_H
#define ENVELOPE_H

#include <stdint.h>

class Envelope
{
public:
	Envelope();
	Envelope(float attackTime, float decayTime, float sustainLevel, float releaseTime);
	float evaluate();
	void attack(uint32_t sampleRate);
	void release();

private:
	static const int ATTACK_STATE = 0;
	static const int DECAY_STATE = 1;
	static const int SUSTAIN_STATE = 2;
	static const int RELEASE_STATE = 3;
	
	int state;
	float timeAcc;
	float timeInc;
	float attackTime;
	float decayTime;
	float sustainLevel;
	float releaseTime;
	float currentLevel;
	float levelAtRelease;
};

#endif
