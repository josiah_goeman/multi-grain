#include <unistd.h>
#define SINE 0
#define TRI 1
#define SAW 2
#define SQUARE 3

enum WaveShapes
{
	SINE,
	TRI,
	SAW,
	SQUARE
};

enum MixModes
{
	FADE,
	SYNC
};

struct VoiceSettings
{
	uint8_t osc1Shape;
	uint8_t osc2Shape;
	int8_t osc12MixBalance;
	uint8_t osc12MixMode;
};
