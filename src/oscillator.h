#ifndef OSCILLATOR_H
#define OSCILLATOR_H

#include <stdint.h>

class Oscillator
{
public:
	Oscillator();
	float evaluate();
	void setFrequency(float newFrequency, uint32_t sampleRate);
	void setShape(int newShape);
	void reset();

private:
    static constexpr float PI = 3.14159265359f;
    static constexpr float TWO_PI = 6.28318530718f;

	static const int SINE_SHAPE = 0;
	static const int TRI_SHAPE = 1;
	static const int SAW_SHAPE = 2;
	static const int SQUARE_SHAPE = 3;

	int shape;
	float phaseAcc;
	float phaseInc;
	float amplitude;
	float decaySpeed;
};

#endif
