#include "image_dial_widget.h"
#include <QWidget>
#include <QPainter>
#include <QPaintEvent>
#include <QStringList>
#include <iostream>

ImageDialWidget::ImageDialWidget(QWidget* parent):QDial(parent)
{
    
}

void ImageDialWidget::paintEvent(QPaintEvent* event)
{
    //do normal overridden behavior
    QDial::paintEvent(event);

    if(!imagesLoaded)
    {
        QStringList iconFileNames = accessibleDescription().split(",");
        for(int i = 0; i < iconFileNames.length(); i++)
        {
            QPixmap pixMap;
            pixMap.load(iconFileNames[i]);
            icons.push_back(pixMap);
        }

        imagesLoaded = true;
    }

	QPainter painter(this);

    int iconWidth = width() / 3;
    int iconHeight = height() / 3;
    painter.drawPixmap(width() / 2 - iconWidth / 2, height() / 4, iconWidth, iconHeight, icons[value()]);
    
    QFont font = painter.font();
    font.setPointSize(10);
    QFontMetrics fm = QFontMetrics(font);
    painter.setFont(font);

    QString label = accessibleName();
    painter.setPen(QColor(20, 20, 20, 255));
    painter.drawText(width() / 2 - fm.width(label) / 2, height() - height() / 4, label);
}
