#include "number_dial_widget.h"
#include "moremath.h"
#include "gui_constants.h"
#include <algorithm>
#include <QWidget>
#include <QPainter>
#include <QPaintEvent>
#include <iostream>

const int GAP_ANGLE = 90;

NumberDialWidget::NumberDialWidget(QWidget* parent):QDial(parent)
{
    
}

void NumberDialWidget::mouseMoveEvent(QMouseEvent* event)
{
    if(capturedMouse)
    {
        pointerDelta += (event->globalX() - pressPos.x()) - (event->globalY() - pressPos.y());
        if(abs(pointerDelta) > 5)
        {
            setValue(value() + (pointerDelta > 0 ? 1 : -1));
            pointerDelta = 0;
        }

        QCursor::setPos(pressPos);
    }
    else
    {

    }
}
void NumberDialWidget::mousePressEvent(QMouseEvent* event)
{
    pointerDelta = 0;
    pressPos = event->globalPos();
    setCursor(Qt::BlankCursor);
    capturedMouse = true;
}
void NumberDialWidget::mouseReleaseEvent(QMouseEvent* event)
{
    setCursor(Qt::ArrowCursor);
    capturedMouse = false;
}

void NumberDialWidget::paintEvent(QPaintEvent* event)
{
    //do normal overridden behavior
    QDial::paintEvent(event);

	QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);


    int radius = std::min(width(), height()) / 2;
    QPoint center(width() / 2, height() / 2);
    QRect bounds(center.x() - radius, center.y() - radius, radius * 2, radius * 2);

    //draw outside circle
    painter.setPen(DARK_METER_COLOR);
    painter.setBrush(DARK_BACKGROUND_COLOR);
    painter.drawEllipse(bounds);
    
    //draw pie
    //remember, angles are in 16ths of a degree
    painter.setPen(QColor(0, 0, 0, 0));
    painter.setBrush(DARK_METER_COLOR);
    int zeroAngle, arcAngle;
    if(minimum() == 0)
    {
        zeroAngle = (270 - GAP_ANGLE / 2);
    }
    else
    {
        zeroAngle = 90;
        arcAngle = -(value() / (float)maximum()) * (180 - GAP_ANGLE / 2);
    }
    painter.drawPie(bounds, zeroAngle * 16, arcAngle * 16);

    //cover up the inside
    painter.setBrush(DARK_BACKGROUND_COLOR);
    painter.drawEllipse(center, radius * 0.75f, radius * 0.75f);

    //draw value
    QFont font = painter.font();
    font.setPointSize(16);
    QFontMetrics fm = QFontMetrics(font);
    painter.setFont(font);
    painter.setPen(DARK_TEXT_COLOR);
    QString valueDisplay = QString::number(value());
    painter.drawText(width() / 2 - fm.width(valueDisplay) / 2, height() / 2 + fm.ascent() / 2, valueDisplay);
    
    //draw label
    font.setPointSize(10);
    fm = QFontMetrics(font);
    painter.setFont(font);
    QString label = accessibleName();
    painter.drawText(width() / 2 - fm.width(label) / 2, height() - height() / 4, label);
}
