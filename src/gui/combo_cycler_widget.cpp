#include "combo_cycler_widget.h"
#include "gui_constants.h"
#include "moremath.h"
#include <QPainter>
#include <iostream>
#include <QMouseEvent>

ComboCyclerWidget::ComboCyclerWidget(QWidget* parent):QComboBox(parent)
{

}

void ComboCyclerWidget::paintEvent(QPaintEvent* event)
{
	if(!loadedIcons)
	{
		for(int i = 0; i < count(); i++)
		{
			QPixmap pixMap;
            pixMap.load(itemText(i));
            icons.push_back(pixMap);
		}
		loadedIcons = true;
	}

	QPainter painter(this);

    //draw selection and icons
	int selectionBoxWidth = width() / count();
	painter.setPen(QColor(0, 0, 0, 0));
	painter.setBrush(DARK_METER_COLOR);
    for(int i = 0; i < count(); i++)
	{
        QRect selectionBox(i * selectionBoxWidth, 0, selectionBoxWidth + 1, height());
        if(i == currentIndex())
            painter.drawRect(selectionBox);
    	selectionBox.adjust(GUI_OUTLINE_WIDTH, GUI_OUTLINE_WIDTH, -GUI_OUTLINE_WIDTH, -GUI_OUTLINE_WIDTH);
        painter.drawPixmap(selectionBox, icons[i]);
    }

    //draw outline box
    painter.setPen(DARK_METER_COLOR);
    painter.setBrush(QColor(0, 0, 0, 0));
    QPen pen = painter.pen();
    pen.setWidth(GUI_OUTLINE_WIDTH);
    painter.setPen(pen);
    painter.drawRect(0, 0, width(), height());
}

void ComboCyclerWidget::mousePressEvent(QMouseEvent* event)
{
    //setCurrentIndex((currentIndex() + count() + (event->buttons() == Qt::LeftButton ? 1 : -1)) % count());
    int xPos = clamp(event->x(), 0, width() - GUI_OUTLINE_WIDTH);
    setCurrentIndex(xPos / (width() / count()));
}

void ComboCyclerWidget::mouseMoveEvent(QMouseEvent* event)
{
    int xPos = clamp(event->x(), 0, width() - GUI_OUTLINE_WIDTH);
    setCurrentIndex(xPos / (width() / count()));
}
