#ifndef TIMBRE_PANEL_H
#define TIMBRE_PANEL_H

#include <QDialog>

namespace Ui {
class TimbrePanel;
}

class TimbrePanel : public QDialog
{
    Q_OBJECT

public:
    explicit TimbrePanel(QWidget *parent = 0);
    ~TimbrePanel();

private slots:
    void on_comboBox_2_currentIndexChanged(int index);

private:
    Ui::TimbrePanel *ui;
};

#endif // TIMBRE_PANEL_H
