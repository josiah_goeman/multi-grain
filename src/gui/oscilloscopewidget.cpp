#include "oscilloscopewidget.h"
#include "gui_constants.h"
#include <QTimer>
#include <QPaintEvent>
#include <QPainter>
#include <QColor>
#include "iostream"

OscilloscopeWidget::OscilloscopeWidget(QWidget* parent):QWidget(parent)
{
	QTimer* timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(update()));
	timer->start((1.0f / 30.0f) * 1000);
}

OscilloscopeWidget::~OscilloscopeWidget()
{
	//delete[] ringBuffer;
}

void OscilloscopeWidget::pushSamples(float* samples, int size)
{

}

void OscilloscopeWidget::setBuffer(float* buffer, int size)
{
	this->buffer = buffer;
	bufferSize = size;
}

void OscilloscopeWidget::paintEvent(QPaintEvent* event)
{
	QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.translate(0, 0);

    painter.setBrush(QColor(0, 0, 0, 255));
    painter.setPen(DARK_METER_COLOR);
    QRect bounds(0, 0, width()-1, height()-1);
    QPen pen = painter.pen();
    pen.setWidth(4);
    painter.setPen(pen);
    painter.drawRoundedRect(bounds, 20, 20);

    float canvasWidth = width();
    float canvasHeight = height();

    int bufferIndex = 1;
    float xInc = (1.0f / (bufferSize-1)) * canvasWidth;
    float xPos = xInc;
    float xPosPrev = 0;

    while(true)
    {
    	if(bufferIndex == bufferSize) break;

    	float midY = canvasHeight / 2.0f;
    	painter.drawLine(xPosPrev, midY - buffer[bufferIndex-1] * (canvasHeight / 2), xPos, midY - buffer[bufferIndex] * (canvasHeight / 2));


    	bufferIndex++;
		xPosPrev = xPos;
    	xPos += xInc;
    }
}
