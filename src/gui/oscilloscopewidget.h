#ifndef OSCILLOSCOPEWIDGET_H
#define OSCILLOSCOPEWIDGET_H

#include <QWidget>

class OscilloscopeWidget : public QWidget
{
	Q_OBJECT

	public:
		OscilloscopeWidget(QWidget* parent = 0);
		~OscilloscopeWidget();
		void pushSamples(float* samples, int size);
		void setBuffer(float* buffer, int size);

	private:
		float* buffer;
		int bufferSize = 0;
		int startIndex;

	protected:
		void paintEvent(QPaintEvent* event) override;
};

#endif
