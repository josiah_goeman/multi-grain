#ifndef IMAGE_DIAL_WIDGET_H
#define IMAGE_DIAL_WIDGET_H

#include <QWidget>
#include <QDial>

using namespace std;

class ImageDialWidget : public QDial
{
	Q_OBJECT

	public:
		ImageDialWidget(QWidget* parent = 0);
		vector<QPixmap> icons;

	protected:
		void paintEvent(QPaintEvent* event) override;

	private:
		bool imagesLoaded = false;
};

#endif
