#ifndef COMBO_CYCLER_WIDGET_H
#define COMBO_CYCLER_WIDGET_H

#include <QComboBox>
#include <vector>

class ComboCyclerWidget : public QComboBox
{
	Q_OBJECT

	public:
		ComboCyclerWidget(QWidget* parent = 0);

	protected:
		void paintEvent(QPaintEvent* event) override;
		void mousePressEvent(QMouseEvent* event) override;
		void mouseMoveEvent(QMouseEvent* event) override;
	private:
		std::vector<QPixmap> icons;
		bool loadedIcons = false;
};

#endif
