#ifndef NUMBER_DIAL_WIDGET_H
#define NUMBER_DIAL_WIDGET_H

#include <QWidget>
#include <QDial>

class NumberDialWidget : public QDial
{
	Q_OBJECT

	public:
		NumberDialWidget(QWidget* parent = 0);

	protected:
		void paintEvent(QPaintEvent* event) override;
		void mouseMoveEvent(QMouseEvent* event) override;
		void mousePressEvent(QMouseEvent* event) override;
		void mouseReleaseEvent(QMouseEvent* event) override;
	private:
		QPoint pressPos;
		bool capturedMouse = false;
		int pointerDelta = 0;
};

#endif
