#ifndef GUI_COLORS_H
#define GUI_COLORS_H

#include <QColor>

#define DARK_BACKGROUND_COLOR QColor(32, 32, 32, 255)
#define DARK_OUTLINE_COLOR QColor(10, 10, 10, 255)
#define DARK_METER_COLOR QColor(170, 57, 57, 255)
#define DARK_TEXT_COLOR QColor(200, 200, 200, 255)

#define GUI_OUTLINE_WIDTH 4

#endif
