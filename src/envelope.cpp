#include "envelope.h"
#include "moremath.h"
#include <iostream>

Envelope::Envelope()
{
	attackTime = 0;
	decayTime = 0.05;
	sustainLevel = 0.25;
	releaseTime = 1;
}

Envelope::Envelope(float attackTime, float decayTime, float sustainLevel, float releaseTime)
{
	this->attackTime = attackTime;
	this->decayTime = decayTime;
	this->sustainLevel = sustainLevel;
	this->releaseTime = releaseTime;
}

float Envelope::evaluate()
{
	timeAcc += timeInc;

	switch(state)
	{
		case ATTACK_STATE:
			if(timeAcc > attackTime)
			{
				state = DECAY_STATE;
				timeAcc = 0;
				currentLevel = 1;
				break;
			}

			currentLevel = lerp(0, 1, timeAcc / attackTime);
			break;

		case DECAY_STATE:
			if(timeAcc > decayTime)
			{
				state = SUSTAIN_STATE;
				currentLevel = sustainLevel;
				break;
			}

			currentLevel = lerp(1, sustainLevel, timeAcc / decayTime);
			break;

		case RELEASE_STATE:
			if(timeAcc > releaseTime)
			{
				currentLevel = -1;
				break;
			}

			currentLevel = lerp(levelAtRelease, 0, timeAcc / releaseTime);
			break;

		default:
			return currentLevel = sustainLevel;
	}

	return currentLevel;
}

void Envelope::attack(uint32_t sampleRate)
{
	state = ATTACK_STATE;
	timeAcc = 0;
	timeInc = 1.0f / sampleRate;
}

void Envelope::release()
{
	state = RELEASE_STATE;
	timeAcc = 0;
	levelAtRelease = currentLevel;
}
