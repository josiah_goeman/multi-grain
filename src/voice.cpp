#include "voice.h"
#include "moremath.h"
#include <iostream>
#include <cstdlib>

Voice::Voice()
{
	noteNumber = 128;	//midi notes are in range 0-127
	state = STATE_OFF;
	pitchBend = 0.5f;
}

void Voice::addToOutput(float* outputBuffer, uint32_t bufferSize)
{
	if(state == STATE_OFF) return;

	for(uint32_t i = 0; i < bufferSize; i++)
	{
		syncPhaseAcc += syncPhaseInc;
		if(syncPhaseAcc > 1.0f)
		{
			syncPhaseAcc -= 1.0f;
			grainOsc1.reset();
			grainOsc2.reset();
		}

		float amplitude = envelope.evaluate();
		if(amplitude == -1.0f)
		{
			state = STATE_OFF;
			return;
		}

		outputBuffer[i] += ((grainOsc1.evaluate())/* + grainOsc2.evaluate()) / 2*/ * amplitude) / 2;
	}
}

void Voice::press(uint8_t noteNumber, uint8_t velocity)
{
	this->noteNumber = noteNumber;
	state = STATE_PRESSED;
	syncPhaseAcc = 0.0f;

	grainOsc1.setFrequency(midiToFrequency(noteNumber) * 1.0f, sampleRate);
	grainOsc1.setShape(1);
	grainOsc2.setFrequency(midiToFrequency(noteNumber) * 2.0f, sampleRate);
	grainOsc2.setShape(3);
	
	updateFrequency();

	envelope.attack(sampleRate);
}

void Voice::release(float velocity)
{
	state = STATE_RELEASING;
	envelope.release();
}

char Voice::getState()
{
	return state;
}

uint8_t Voice::getId()
{
	return noteNumber;
}

void Voice::setPitchBend(float newPitchBend)
{
	this->pitchBend = newPitchBend;
	updateFrequency();
}

void Voice::updateFrequency()
{
	float frequency = lerp(
		midiToFrequency(noteNumber - 2),
		midiToFrequency(noteNumber + 2),
		pitchBend);
	syncPhaseInc = frequency / sampleRate;
}

void setADSR(float attackTime, float decayTime, float sustainLevel, float releaseLevel)
{

}
