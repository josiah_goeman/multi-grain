/********************************************************************************
** Form generated from reading UI file 'timbre_panel.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TIMBRE_PANEL_H
#define UI_TIMBRE_PANEL_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "combo_cycler_widget.h"
#include "number_dial_widget.h"

QT_BEGIN_NAMESPACE

class Ui_TimbrePanel
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    ComboCyclerWidget *comboBox_2;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_2;
    ComboCyclerWidget *comboBox_3;
    QHBoxLayout *horizontalLayout;
    NumberDialWidget *dial;
    NumberDialWidget *dial_2;
    NumberDialWidget *dial_3;

    void setupUi(QDialog *TimbrePanel)
    {
        if (TimbrePanel->objectName().isEmpty())
            TimbrePanel->setObjectName(QString::fromUtf8("TimbrePanel"));
        TimbrePanel->resize(654, 316);
        verticalLayoutWidget = new QWidget(TimbrePanel);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(60, 30, 321, 231));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(label);

        comboBox_2 = new ComboCyclerWidget(verticalLayoutWidget);
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(comboBox_2->sizePolicy().hasHeightForWidth());
        comboBox_2->setSizePolicy(sizePolicy1);

        horizontalLayout_2->addWidget(comboBox_2);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_2 = new QLabel(verticalLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy);

        horizontalLayout_3->addWidget(label_2);

        comboBox_3 = new ComboCyclerWidget(verticalLayoutWidget);
        comboBox_3->setObjectName(QString::fromUtf8("comboBox_3"));
        sizePolicy1.setHeightForWidth(comboBox_3->sizePolicy().hasHeightForWidth());
        comboBox_3->setSizePolicy(sizePolicy1);

        horizontalLayout_3->addWidget(comboBox_3);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        dial = new NumberDialWidget(verticalLayoutWidget);
        dial->setObjectName(QString::fromUtf8("dial"));
        dial->setMinimum(-100);
        dial->setMaximum(100);
        dial->setNotchesVisible(false);

        horizontalLayout->addWidget(dial);

        dial_2 = new NumberDialWidget(verticalLayoutWidget);
        dial_2->setObjectName(QString::fromUtf8("dial_2"));
        dial_2->setMinimum(-60);
        dial_2->setMaximum(60);
        dial_2->setNotchesVisible(false);

        horizontalLayout->addWidget(dial_2);

        dial_3 = new NumberDialWidget(verticalLayoutWidget);
        dial_3->setObjectName(QString::fromUtf8("dial_3"));
        dial_3->setMinimum(-500);
        dial_3->setMaximum(500);
        dial_3->setNotchesVisible(false);

        horizontalLayout->addWidget(dial_3);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(TimbrePanel);

        QMetaObject::connectSlotsByName(TimbrePanel);
    } // setupUi

    void retranslateUi(QDialog *TimbrePanel)
    {
        TimbrePanel->setWindowTitle(QApplication::translate("TimbrePanel", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("TimbrePanel", "OSC1", 0, QApplication::UnicodeUTF8));
        comboBox_2->clear();
        comboBox_2->insertItems(0, QStringList()
         << QApplication::translate("TimbrePanel", "icons/sine_icon.svg.png", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("TimbrePanel", "icons/triangle_icon.svg.png", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("TimbrePanel", "icons/saw_icon.svg.png", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("TimbrePanel", "icons/pulse_icon.svg.png", 0, QApplication::UnicodeUTF8)
        );
#ifndef QT_NO_ACCESSIBILITY
        comboBox_2->setAccessibleName(QApplication::translate("TimbrePanel", "OSC1 Shape", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_ACCESSIBILITY
        label_2->setText(QApplication::translate("TimbrePanel", "OSC2", 0, QApplication::UnicodeUTF8));
        comboBox_3->clear();
        comboBox_3->insertItems(0, QStringList()
         << QApplication::translate("TimbrePanel", "icons/sine_icon.svg.png", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("TimbrePanel", "icons/triangle_icon.svg.png", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("TimbrePanel", "icons/saw_icon.svg.png", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("TimbrePanel", "icons/pulse_icon.svg.png", 0, QApplication::UnicodeUTF8)
        );
#ifndef QT_NO_ACCESSIBILITY
        comboBox_3->setAccessibleName(QApplication::translate("TimbrePanel", "OSC1 Shape", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        dial->setAccessibleName(QApplication::translate("TimbrePanel", "Mix", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        dial_2->setAccessibleName(QApplication::translate("TimbrePanel", "Semitones", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        dial_3->setAccessibleName(QApplication::translate("TimbrePanel", "Fine", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_ACCESSIBILITY
    } // retranslateUi

};

namespace Ui {
    class TimbrePanel: public Ui_TimbrePanel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TIMBRE_PANEL_H
